import { Component, ViewChild } from '@angular/core';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NavController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { Storage } from '@ionic/storage';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { File } from '@ionic-native/file/ngx';

const MEDIA_FILES_KEY = 'mediaFiles';
@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage {
	clickedImage: string;

	options: CameraOptions = {
		quality: 30,
		destinationType: this.camera.DestinationType.DATA_URL,
		encodingType: this.camera.EncodingType.JPEG,
		mediaType: this.camera.MediaType.PICTURE
	}

	mediaFiles = [];
	@ViewChild('myvideo') myVideo: any;

	constructor(private camera: Camera,
		public navCtrl: NavController, public mediaCapture: MediaCapture, private storage: Storage, private file: File, private media: Media,
		public socialSharing: SocialSharing) {
	}

	captureImage() {
		this.camera.getPicture(this.options).then((imageData) => {
			// imageData is either a base64 encoded string or a file URI
			// If it's base64 (DATA_URL):
			let base64Image = 'data:image/jpeg;base64,' + imageData;
			this.clickedImage = base64Image;
		}, (err) => {
			console.log(err);
			// Handle error
		});
	}

	ionViewDidLoad() {
		this.storage.get(MEDIA_FILES_KEY).then(res => {
			this.mediaFiles = JSON.parse(res) || [];
		})
	}

	captureAudio() {
		this.mediaCapture.captureAudio().then(res => {
			this.storeMediaFiles(res);
		}, (err: CaptureError) => console.error(err));
	}


	captureVideo() {
		let options: CaptureVideoOptions = {
			limit: 1,
			duration: 30
		}
		this.mediaCapture.captureVideo(options).then((res: MediaFile[]) => {
			let capturedFile = res[0];
			let fileName = capturedFile.name;
			let dir = capturedFile['localURL'].split('/');
			dir.pop();
			let fromDirectory = dir.join('/');
			var toDirectory = this.file.dataDirectory;

			this.file.copyFile(fromDirectory, fileName, toDirectory, fileName).then((res) => {
				this.storeMediaFiles([{ name: fileName, size: capturedFile.size }]);
			}, err => {
				console.log('err: ', err);
			});
		},
			(err: CaptureError) => console.error(err));
	}

	play(myFile) {
		if (myFile.name.indexOf('.wav') > -1) {
			const audioFile: MediaObject = this.media.create(myFile.localURL);
			audioFile.play();
		} else {
			let path = this.file.dataDirectory + myFile.name;
			let url = path.replace(/^file:\/\//, '');
			let video = this.myVideo.nativeElement;
			video.src = url;
			video.play();
		}
	}

	storeMediaFiles(files) {
		this.storage.get(MEDIA_FILES_KEY).then(res => {
			if (res) {
				let arr = JSON.parse(res);
				arr = arr.concat(files);
				this.storage.set(MEDIA_FILES_KEY, JSON.stringify(arr));
			} else {
				this.storage.set(MEDIA_FILES_KEY, JSON.stringify(files))
			}
			this.mediaFiles = this.mediaFiles.concat(files);
		})
	}

	captureAudio_() {
		this.mediaCapture.captureAudio().then((audio: MediaFile[]) => {
			this.shareMedia("audio file capture by media capture plugin", "media capture", audio[0].fullPath, "");
		}, (err) => {
			console.log('err:', err)
		})
	}

	captureImage_() {
		this.mediaCapture.captureImage().then((image: MediaFile[]) => {
			this.shareMedia("image file capture by media capture plugin", "media capture", image[0].fullPath, "");
		}, (err) => {
			console.log('err:', err)
		})
	}

	captureVideo_() {
		this.mediaCapture.captureVideo().then((video: MediaFile[]) => {
			this.shareMedia("video file capture by media capture plugin", "media capture", video[0].fullPath, "");
		}, (err) => {
			console.log('err:', err)
		})
	}

	shareMedia(message, subject, filepath, url) {
		this.socialSharing.share(message, subject, filepath, url).then(() => {

		}, (err) => {
			console.log('err:', err)
		});
	}
}
